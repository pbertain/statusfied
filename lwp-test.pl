#!/usr/bin/perl

use strict;
use warnings;

# Create a user agent object
use LWP::UserAgent;

my $ua = LWP::UserAgent->new;
$ua->agent("GreenLanternCorp/28.14 ");

# Create a request
my $req = HTTP::Request->new(GET => 'http://web.aws.bertain.net/airpuff/airpuff.html');
#my $req = HTTP::Status->new(GET => 'http://web.aws.bertain.net/airpuff');
#$req->content_type('application/x-www-form-urlencoded');
#$req->content('query=libwww-perl&mode=dist');

# Pass request to the user agent and get a response back
my $res = $ua->request($req);

# Check the outcome of the response
if ($res->is_success) {
	print $res->status_line, "\n";
}
else {
	print $res->status_line, "\n";
}
