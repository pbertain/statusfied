#!/usr/bin/perl

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Headers;
use Mozilla::CA;

# Data format:
# QA Env, Partition, Site,VIP

# Create a user agent object
my $user_agent = LWP::UserAgent->new();
$user_agent->agent("ISG Delivery Ops/CurlyFries ");
$user_agent->max_redirect(1);
	
#print "iC1 - Partition 1 - icloud.com --> ";
print "web.aws.bertain.net --> ";

#my $fullURL = "http://17.176.240.40/imovietheateratv/";
my $fullURL = "http://web.aws.bertain.net/airpuff/airpuff.html";

# Create a request
my $req = HTTP::Request->new(
	#new HTTP::Request(
	GET => "$fullURL",
	new HTTP::Headers(
		Host => "www.bertain.net",
		'X-Curly-Fries-Plop' => "getting-jiggy",
	)
);

# Pass request to the user agent and get a response back
my $res = $user_agent->request($req);

# Check the outcome of the response
print $res->status_line, "\n";

exit 0;

