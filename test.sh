#!/bin/sh

curl -sIkH'Host: icloud.com' http://17.176.240.40/imovietheateratv/       | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: icloud.com' https://17.176.240.40/imovietheateratv/      | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' http://17.176.240.49/imovietheateratv/   | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' https://17.176.240.49/imovietheateratv/  | egrep "(HTTP|Location)" # Expect 404 (for now)
curl -sIkH'Host: icloud.com' http://17.176.240.76/imovietheateratv/       | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: icloud.com' https://17.176.240.76/imovietheateratv/      | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' http://17.176.240.85/imovietheateratv/   | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' https://17.176.240.85/imovietheateratv/  | egrep "(HTTP|Location)" # Expect 404 (for now)

curl -sIkH'Host: icloud.com' http://17.176.242.122/imovietheateratv/       | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: icloud.com' https://17.176.242.122/imovietheateratv/      | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' http://17.176.242.149/imovietheateratv/   | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' https://17.176.242.149/imovietheateratv/  | egrep "(HTTP|Location)" # Expect 404 (for now)
curl -sIkH'Host: icloud.com' http://17.176.242.175/imovietheateratv/       | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: icloud.com' https://17.176.242.175/imovietheateratv/      | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' http://17.176.242.187/imovietheateratv/   | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' https://17.176.242.187/imovietheateratv/  | egrep "(HTTP|Location)" # Expect 404 (for now)

curl -sIkH'Host: icloud.com' http://17.176.244.44/imovietheateratv/       | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: icloud.com' https://17.176.244.44/imovietheateratv/      | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' http://17.176.244.56/imovietheateratv/   | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' https://17.176.244.56/imovietheateratv/  | egrep "(HTTP|Location)" # Expect 404 (for now)
curl -sIkH'Host: icloud.com' http://17.176.244.82/imovietheateratv/       | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: icloud.com' https://17.176.244.82/imovietheateratv/      | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' http://17.176.244.94/imovietheateratv/   | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' https://17.176.244.94/imovietheateratv/  | egrep "(HTTP|Location)" # Expect 404 (for now)

curl -sIkH'Host: icloud.com' http://17.176.246.34/imovietheateratv/       | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: icloud.com' https://17.176.246.34/imovietheateratv/      | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' http://17.176.246.46/imovietheateratv/   | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' https://17.176.246.46/imovietheateratv/  | egrep "(HTTP|Location)" # Expect 404 (for now)
curl -sIkH'Host: icloud.com' http://17.176.246.72/imovietheateratv/       | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: icloud.com' https://17.176.246.72/imovietheateratv/      | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' http://17.176.246.84/imovietheateratv/   | egrep "(HTTP|Location)" # Expect 301
curl -sIkH'Host: www.icloud.com' https://17.176.246.84/imovietheateratv/  | egrep "(HTTP|Location)" # Expect 404 (for now)

