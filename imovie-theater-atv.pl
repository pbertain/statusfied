#!/usr/bin/perl

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Headers;
use Mozilla::CA;

my $data = "env-data.txt";
my $line_no = 1;
#my $requestedURL = "imovietheateratv/index.html";

# Data format:
# QA Env, Partition, Site,VIP

open DATA, $data || die $!;
while (<DATA>) {
	chomp(my ($qa_env, $partition, $website_name, $requestedURL, $vip_addr) = split(/,/, $_));

	# Create a user agent object
	my $user_agent = LWP::UserAgent->new(
			ssl_opts => { verify_hostname => 0 },
	);
	$user_agent->agent("ISG Delivery Ops/CurlyFries ");
	$user_agent->max_redirect(0);
#	$user_agent->header( Host => $website_name );
	my $headers = HTTP::Headers->new( Host => "$website_name");

	my @proto = qw/http https/;

	for my $proto (@proto) {
		my $fullURL = "$proto://$vip_addr/$requestedURL";

		printf "%-3s/%-11s/%-14s --> ", $qa_env, "Partition $partition", $website_name;
		printf "%-51s --> %-14s --> ", $fullURL, $website_name;

		# Create a request
		my $req = HTTP::Request->new(
			#new HTTP::Request(
				GET => "$fullURL",
				new HTTP::Headers(
					Host => "$website_name",
				)
			#)
		);

		# Pass request to the user agent and get a response back
		my $res = $user_agent->request($req);

		# Check the outcome of the response
		print $res->status_line, "\n";
	}
}

exit 0;

